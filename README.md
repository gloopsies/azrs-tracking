![](./resources/icons/rs.radojevic.game_night.svg)

# Game night

Projekat iz predmeta računarska grafika na Matematičkom fakultetu

## Implementirani efekti:
- Grupa A:
  - [x] Anti-aliasing
- Grupa B:
  - [x] HDR
  - [x] Bloom
  - [x] Point shadows

## Pokretanje:

Preferirani metod za pokretanje projekta je uz pomoć `meson` alata ili `flatpak` fajla, ali takođe je implementiran i `CMakeListst.txt` fajl za lakše pokretanje na starijim CLion instalacijama.

## Setup

### Meson

```shell
$ cp .env.example .env
$ ./tools/setup_project.sh
$ meson install -C buildDir/
$ ./buildDir/src/game_night
```

### Flatpak
```shell
$ flatpak-builder --repo=repo --force-clean build-dir rs.radojevic.game_night.json
$ flatpak run rs.radojevic.game_night
```

### CMake
```shell
$ mkdir build
$ cd build
$ cmake ..
$ make
$ cd ..
$ ./game_night
```

Svi modeli preuzeti su sa: https://sketchfab.com/feed

![](./resources/game_night.gif)
