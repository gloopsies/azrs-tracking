import os

from conan import ConanFile
from conan.tools.files import copy


class GameNight(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "PkgConfigDeps", "MesonToolchain"

    def requirements(self):
        self.requires("glfw/3.3.8")
        self.requires("glad/0.1.36")
        self.requires("assimp/5.2.2")
        self.requires("freetype/2.13.2")
        self.requires("imgui/1.89.9")
        self.requires("glm/0.9.9.8")
        self.requires("opengl/system")

        self.requires("libxml2/2.12.3", override=True)


    def generate(self):
        imgui_dir = os.path.join(self.dependencies["imgui"].package_folder, "res", "bindings")
        bindings_dir = os.path.join(self.source_folder, os.getenv("BUILD_DIR", ".") + "/bindings")

        copy(self, "*glfw*", imgui_dir, bindings_dir)
        copy(self, "*opengl3*", imgui_dir, bindings_dir)

    def configure(self):
        self.options['glad'].gl_profile = 'core'
        self.options['glad'].gl_version = '3.3'
        # self.options['glfw'].with_x11 = False
        # self.options['glfw'].with_wayland = True
