ARG ALPINE_VERSION=3.19.1
ARG BASE_IMAGE=docker.io/alpine:${ALPINE_VERSION}
ARG BUILD_DIRECTORY=/build

FROM ${BASE_IMAGE} AS build

RUN apk update
RUN apk add \
    meson ninja make cmake g++ python3 py3-pip pkgconfig mesa-gl mesa-dev \
    libfontenc-dev libx11-dev libice-dev libsm-dev libxaw-dev libxcomposite-dev libxcursor-dev libxi-dev \
    libxinerama-dev libxkbfile-dev libxrandr-dev libxres-dev libxscrnsaver-dev libxtst-dev libxv-dev libxvmc-dev \
    libxcb-dev xcb-util-wm-dev xcb-util-image-dev xcb-util-keysyms-dev xcb-util-renderutil-dev xcb-util-cursor-dev \
    clang17-extra-tools

RUN adduser -D build_user

RUN mkdir /src /build /app
RUN chown -R build_user /src /build /app

WORKDIR /src
USER build_user:build_user

ENV VIRTUAL_ENV="/home/build_user/venv"
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN pip install conan

COPY conanfile.py .
RUN conan profile detect

ARG BUILD_DIRECTORY
RUN env BUILD_DIR="$BUILD_DIRECTORY" conan install . --output-folder=/build --build=missing --settings compiler.version=13.2

COPY meson.build ./
COPY meson.options ./
COPY src ./src
COPY libs ./libs
COPY resources ./resources

WORKDIR $BUILD_DIRECTORY
RUN meson setup --reconfigure --native-file conan_meson_native.ini -Dprefix="/app" /src .
RUN meson compile
RUN meson install