ARG ALPINE_VERSION=3.19.1
ARG BASE_IMAGE=docker.io/alpine:${ALPINE_VERSION}
ARG BUILD_DIRECTORY=/build
ARG BUILD_IMAGE=game_night/build

FROM ${BUILD_IMAGE} AS build
FROM ${BASE_IMAGE} AS runtime

RUN apk update
RUN apk add xterm mesa libstdc++ mesa-utils xf86-video-ati mesa-demos

RUN adduser -D run_user
WORKDIR /run
RUN chown -R run_user:run_user /run
USER run_user

RUN mkdir build
COPY --from=build /app /app

WORKDIR /app
CMD ["/app/bin/game_night"]
