#ifndef BUILDER_H
#define BUILDER_H

#define BUILDER1(TYPE)                                  \
   protected:                                           \
    class Builder;                                      \
                                                        \
   public:                                              \
    static Builder *builder() { return new Builder(); } \
                                                        \
   protected:                                           \
    class Builder {                                     \
       public:                                          \
        virtual ~Builder() = default;                   \
        virtual TYPE *build();

#define BUILDER2(TYPE, EXTENDS)                         \
   protected:                                           \
    class Builder;                                      \
                                                        \
   public:                                              \
    static Builder *builder() { return new Builder(); } \
                                                        \
   protected:                                           \
    class Builder : public EXTENDS {                    \
       public:                                          \
        ~Builder() override = default;                  \
        TYPE *build() override;

#define BUILDER(...) BUILDERX(__VA_ARGS__, BUILDER2, BUILDER1)(__VA_ARGS__)
#define BUILDERX(a, b, c, ...) c

#define BUILDER_END \
    }               \
    ;

#define BUILDER_NEW(FIELD_TYPE, FIELD_NAME, DEFAULT_VALUE)                                        \
   private:                                                                                       \
    FIELD_TYPE $##FIELD_NAME = DEFAULT_VALUE;                                                     \
                                                                                                  \
   protected:                                                                                     \
    void set_##FIELD_NAME(FIELD_TYPE _##FIELD_NAME) { $##FIELD_NAME = std::move(_##FIELD_NAME); } \
    FIELD_TYPE get_##FIELD_NAME() { return $##FIELD_NAME; }                                       \
                                                                                                  \
   public:                                                                                        \
    virtual Builder *FIELD_NAME(FIELD_TYPE _##FIELD_NAME) {                                       \
        $##FIELD_NAME = std::move(_##FIELD_NAME);                                                 \
        return this;                                                                              \
    }

#define BUILDER_OVERRIDE(FIELD_TYPE, FIELD_NAME)                     \
   public:                                                           \
    virtual Builder *FIELD_NAME(FIELD_TYPE _##FIELD_NAME) override { \
        set_##FIELD_NAME(_##FIELD_NAME);                             \
        return this;                                                 \
    }

#endif  // BUILDER_H
