#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <glm/glm.hpp>

#include "constants.hpp"
#include "node.hpp"

constexpr float SPEED       = 2.5f;
constexpr float SENSITIVITY = 0.1f;
constexpr float ZOOM        = 45.0f;

namespace gl {
    class Camera : public Node {
        float zoom{};
        bool active = true;
        float near  = 0.1f;
        float far   = 100.0f;

       protected:
        Camera() = default;

        void draw(float delta, gl::Program *shader) override;

       public:
        std::string get_node_type() override { return "camera"; }

        [[nodiscard]] glm::mat4 getView() const;
        [[nodiscard]] glm::mat4 getProjection(float aspect_ratio) const;

        [[nodiscard]] float get_zoom() const;
        void set_zoom(float zoom);

        [[nodiscard]] bool get_active() const;
        void set_active(bool active);

        [[nodiscard]] float get_near() const;
        void set_near(float near);

        [[nodiscard]] float get_far() const;
        void set_far(float far);

        BUILDER(Camera, Node::Builder)
            BUILDER_NEW(float, zoom, ZOOM)
            BUILDER_NEW(bool, active, false)
            BUILDER_NEW(float, near, 0.1f)
            BUILDER_NEW(float, far, 100.0f)

            BUILDER_OVERRIDE(std::string, name)
            BUILDER_OVERRIDE(glm::vec3, position)
            BUILDER_OVERRIDE(glm::quat, rotation)
            BUILDER_OVERRIDE(glm::vec3, scale)
            BUILDER_OVERRIDE(std::vector<Node *>, children)
        BUILDER_END
    };

    class PlayerCamera : public Camera {
        float movement_speed{};
        float mouse_sensitivity{};

       protected:
        PlayerCamera() = default;

       public:
        std::string get_node_type() override { return "player_camera"; }

        void process_movement(float xoffset, float yoffset);

        [[nodiscard]] float get_movement_speed() const;
        void set_movement_speed(float movement_speed);

        [[nodiscard]] float get_mouse_sensitivity() const;
        void set_mouse_sensitivity(float sensitivity);

        BUILDER(PlayerCamera, Camera::Builder)
            BUILDER_NEW(float, movement_speed, SPEED)
            BUILDER_NEW(float, mouse_sensitivity, SENSITIVITY)

            BUILDER_OVERRIDE(std::string, name)
            BUILDER_OVERRIDE(glm::vec3, position)
            BUILDER_OVERRIDE(glm::quat, rotation)
            BUILDER_OVERRIDE(glm::vec3, scale)
            BUILDER_OVERRIDE(std::vector<Node *>, children)

            BUILDER_OVERRIDE(float, zoom)
            BUILDER_OVERRIDE(bool, active)
            BUILDER_OVERRIDE(float, near)
            BUILDER_OVERRIDE(float, far)
        BUILDER_END
    };
}  // namespace gl

#endif  // CAMERA_HPP
