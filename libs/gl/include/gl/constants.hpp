#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

#include <glm/glm.hpp>

namespace gl {
    inline glm::vec3 up{0, 1, 0};
    inline glm::vec3 front{0, 0, -1};
    inline glm::vec3 right{1, 0, 0};
}  // namespace gl

#endif  // CONSTANTS_HPP
