#ifndef LIGHTS_HPP
#define LIGHTS_HPP

#include <array>
#include <gl/node.hpp>

constexpr int DIRECTIONAL_LIGHTS_MAX = 10;
constexpr int POINT_LIGHTS_MAX       = 10;
constexpr int SPOT_LIGHTS_MAX        = 10;

namespace gl {
    class DirectionalLight : public Node {
        // TODO: color
        glm::vec3 ambient{};
        glm::vec3 diffuse{};
        glm::vec3 specular{};

        // TODO: finish implementing GL light array
        bool is_directional;
        int lights_index                                                            = 0;
        inline static int lights_len                                                = 0;
        inline static std::array<DirectionalLight *, DIRECTIONAL_LIGHTS_MAX> lights = {};

       protected:
        explicit DirectionalLight(bool is_directional);

       public:
        std::string get_node_type() override { return "light"; }

        [[nodiscard]] const glm::vec3 &get_ambient() const;
        void set_ambient(const glm::vec3 &ambient);

        [[nodiscard]] const glm::vec3 &get_diffuse() const;
        void set_diffuse(const glm::vec3 &diffuse);

        [[nodiscard]] const glm::vec3 &get_specular() const;
        void set_specular(const glm::vec3 &specular);

        ~DirectionalLight() override;

        BUILDER(DirectionalLight, Node::Builder)
            BUILDER_NEW(glm::vec3, ambient, {})
            BUILDER_NEW(glm::vec3, diffuse, {})
            BUILDER_NEW(glm::vec3, specular, {})

            BUILDER_OVERRIDE(std::string, name)
            BUILDER_OVERRIDE(glm::vec3, position)
            BUILDER_OVERRIDE(glm::quat, rotation)
            BUILDER_OVERRIDE(glm::vec3, scale)
            BUILDER_OVERRIDE(std::vector<Node *>, children)
        BUILDER_END
    };

    class PointLight : public DirectionalLight {
        float constant{};
        float linear{};
        float quadratic{};

        bool is_point;
        int lights_index                                                = 0;
        inline static int lights_len                                    = 0;
        inline static std::array<PointLight *, POINT_LIGHTS_MAX> lights = {};

       protected:
        explicit PointLight(bool is_point);

       public:
        std::string get_node_type() override { return "point_light"; }

        [[nodiscard]] float get_constant() const;
        void set_constant(float constant);

        [[nodiscard]] float get_linear() const;
        void set_linear(float linear);

        [[nodiscard]] float get_quadratic() const;
        void set_quadratic(float quadratic);

        ~PointLight() override;

        BUILDER(PointLight, DirectionalLight::Builder)
            BUILDER_NEW(float, constant, {})
            BUILDER_NEW(float, linear, {})
            BUILDER_NEW(float, quadratic, {})

            BUILDER_OVERRIDE(std::string, name)
            BUILDER_OVERRIDE(glm::vec3, position)
            BUILDER_OVERRIDE(glm::quat, rotation)
            BUILDER_OVERRIDE(glm::vec3, scale)
            BUILDER_OVERRIDE(std::vector<Node *>, children)

            BUILDER_OVERRIDE(glm::vec3, ambient)
            BUILDER_OVERRIDE(glm::vec3, diffuse)
            BUILDER_OVERRIDE(glm::vec3, specular)
        BUILDER_END
    };

    class SpotLight : public PointLight {
        float inner_cutoff{};
        float outer_cutoff{};

        bool is_spot;
        int lights_index                                              = 0;
        inline static int lights_len                                  = 0;
        inline static std::array<SpotLight *, SPOT_LIGHTS_MAX> lights = {};

       protected:
        explicit SpotLight(bool is_spot);

       public:
        std::string get_node_type() override { return "spot_light"; }

        [[nodiscard]] float get_inner_cutoff() const;
        void set_inner_cutoff(float _inner_cutoff);

        [[nodiscard]] float get_outer_cutoff() const;
        void set_outer_cutoff(float _outer_cutoff);

        ~SpotLight() override;

        BUILDER(SpotLight, PointLight::Builder)
            BUILDER_NEW(float, inner_cutoff, {})
            BUILDER_NEW(float, outer_cutoff, {})

            BUILDER_OVERRIDE(std::string, name)
            BUILDER_OVERRIDE(glm::vec3, position)
            BUILDER_OVERRIDE(glm::quat, rotation)
            BUILDER_OVERRIDE(glm::vec3, scale)
            BUILDER_OVERRIDE(std::vector<Node *>, children)

            BUILDER_OVERRIDE(glm::vec3, ambient)
            BUILDER_OVERRIDE(glm::vec3, diffuse)
            BUILDER_OVERRIDE(glm::vec3, specular)

            BUILDER_OVERRIDE(float, constant)
            BUILDER_OVERRIDE(float, linear)
            BUILDER_OVERRIDE(float, quadratic)
        BUILDER_END
    };
}  // namespace gl

#endif  // LIGHTS_HPP
