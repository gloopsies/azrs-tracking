#ifndef MESH_HPP
#define MESH_HPP

#include <gl/shader.hpp>
#include <gl/texture.hpp>
#include <gl/vertex.hpp>
#include <vector>

namespace gl {
    class Mesh {
        std::vector<Vertex> vertices;
        std::vector<unsigned int> indices;
        std::vector<Texture> textures;
        std::string glslIdentifierPrefix;

        unsigned int VAO = 0, VBO = 0, EBO = 0;

       public:
        Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices,
             std::vector<Texture> textures);

        void draw(const Program &shader) const;

        void set_identifier_prefix(std::string prefix);

       private:
        void setupMesh();
    };
}  // namespace gl

#endif  // MESH_HPP
