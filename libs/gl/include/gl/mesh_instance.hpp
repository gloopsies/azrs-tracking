#ifndef MESH_INSTANCE_HPP
#define MESH_INSTANCE_HPP

#include <gl/model.hpp>
#include <gl/node.hpp>

namespace gl {
    class MeshInstance : public Node {
        Model *model{};

       public:
        ~MeshInstance() override;

        std::string get_node_type() override { return "mesh_instance"; }

        void _draw(float delta, gl::Program *shader) override;

        [[nodiscard]] gl::Model *get_model() const;
        void set_model(gl::Model *_model);

        BUILDER(MeshInstance, Node::Builder)
            BUILDER_NEW(Model *, model, nullptr)

            BUILDER_OVERRIDE(std::string, name)
            BUILDER_OVERRIDE(glm::vec3, position)
            BUILDER_OVERRIDE(glm::quat, rotation)
            BUILDER_OVERRIDE(glm::vec3, scale)
            BUILDER_OVERRIDE(std::vector<Node *>, children)
        BUILDER_END
    };
}  // namespace gl

#endif  // MESH_INSTANCE_HPP
