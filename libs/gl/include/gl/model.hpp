#ifndef MODEL_HPP
#define MODEL_HPP

#include <assimp/scene.h>

#include <array>
#include <gl/builder.hpp>
#include <gl/mesh.hpp>
#include <gl/shader.hpp>
#include <gl/texture.hpp>
#include <string>
#include <vector>

enum POSITION : std::uint8_t { FRONT, CENTER, BACK, DEFAULT };

namespace gl {
    class Model {
        std::vector<Mesh> meshes;
        std::vector<Texture> loaded_textures;

        std::array<glm::vec3, 2> bounding_box{};
        glm::vec3 reference_point{};

        std::string directory;

       public:
        explicit Model(const std::string& path, enum POSITION x, enum POSITION y, enum POSITION z);
        void draw(const Program& shader) const;
        void set_shader_prefix(const std::string& prefix);

        [[nodiscard]] glm::vec3 get_reference_point() const;
        void set_reference_point(glm::vec3 _reference_point);

        BUILDER(Model)
            BUILDER_NEW(std::string, path, {})
            BUILDER_NEW(std::string, shader_prefix, {})

           private:
            enum POSITION $x = DEFAULT, $y = DEFAULT, $z = DEFAULT;

           public:
            Builder* reference_point(enum POSITION x, enum POSITION y, enum POSITION z);
            Builder* reference_point(enum POSITION position = CENTER);

        BUILDER_END

       private:
        void load(const std::string& path);
        void processNode(const aiNode* node, const aiScene* scene);
        Mesh processMesh(aiMesh* mesh, const aiScene* scene);
        void loadTextureMaterial(const aiMaterial* mat, aiTextureType type,
                                 const std::string& typeName, std::vector<Texture>& textures);
    };
}  // namespace gl

#endif  // MODEL_HPP
