#ifndef NODE_HPP
#define NODE_HPP

#include <gl/builder.hpp>
#include <gl/constants.hpp>
#include <glm/gtx/quaternion.hpp>

#include "shader.hpp"

namespace gl {
    class Node {
        Node *parent = nullptr;

        std::string name;
        glm::vec3 position = glm::vec3(0);
        glm::quat rotation = glm::quat();
        glm::vec3 scale    = glm::vec3(1);
        std::vector<Node *> children;

       protected:
        Node() = default;

        virtual void key_clicked(int key, int scancode, int action, int mods) {}
        virtual void key_pressed(double delta) {}
        virtual void mouse_moved(double xpos, double ypos) {}

       public:
        virtual std::string get_node_type() { return "node"; }

        void set_name(std::string name);

        std::vector<Node *> get_children();
        void add_child(Node *child);
        void add_children(const std::vector<Node *> &children);
        void set_children(std::vector<Node *> children);

        [[nodiscard]] glm::vec3 get_position() const;
        [[nodiscard]] glm::vec3 get_world_position() const;
        void set_position(const glm::vec3 &position);
        void move(glm::vec3 move_by);

        [[nodiscard]] const glm::quat &get_rotation() const;
        void set_rotation(const glm::quat &rotation);
        void rotate(glm::vec3 rotation);
        void rotate_around(glm::vec3 vector, glm::vec3 point, float angle);
        void rotate_around(glm::vec3 vector, float angle);
        void lookAt(Node &other);

        [[nodiscard]] const glm::vec3 &get_scale() const;
        void set_scale(const glm::vec3 &scale);

        [[nodiscard]] glm::mat4 get_transform() const;
        [[nodiscard]] glm::mat4 get_world_transform() const;

        [[nodiscard]] glm::vec3 front() const;
        [[nodiscard]] glm::vec3 right() const;
        [[nodiscard]] glm::vec3 up() const;

        Node *get(const std::string &_name);
        void print(const std::string &prefix = "", int last = true);

        virtual void _draw(float delta, gl::Program *shader);
        virtual void draw(float delta, gl::Program *shader) {}

        void _key_clicked(int key, int scancode, int action, int mods);
        void _key_pressed(double delta);
        void _mouse_moved(double xpos, double ypos);

        virtual ~Node();

        BUILDER(Node)
            BUILDER_NEW(std::string, name, {})
            BUILDER_NEW(glm::vec3, position, glm::vec3(0))
            BUILDER_NEW(glm::quat, rotation, glm::quat())
            BUILDER_NEW(glm::vec3, scale, glm::vec3(1))
            BUILDER_NEW(std::vector<Node *>, children, {})
        BUILDER_END
    };
}  // namespace gl

#endif  // NODE_HPP
