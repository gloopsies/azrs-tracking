#ifndef PROJECT_HPP
#define PROJECT_HPP

#include <gl/scene.hpp>
#include <gl/window.hpp>
#include <string>

namespace gl {
    class Project {
        static std::string name;
        static gl::Window *window;
        static gl::Scene *active_scene;

       public:
        static gl::Window *get_window();

        static void init(std::string _name, int width, int height);
        static void set_scene(gl::Scene *scene);
        static void run();
        static void close();
        static bool key_clicked(int key);
        static void finalize();
    };
}  // namespace gl

#endif  // PROJECT_HPP
