#ifndef SCENE_HPP
#define SCENE_HPP

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#include <array>
#include <gl/camera.hpp>
#include <gl/node.hpp>
#include <gl/quad.hpp>
#include <gl/shader.hpp>
#include <gl/window.hpp>

namespace gl {
    class Scene : public Node {
        gl::Program *shader_program;

        struct {
            gl::Program *shader;
            unsigned FBO;
            std::array<unsigned, 2> colorBuffers;
            unsigned depthBuffer;
            float exposure = 3.0f;
            bool enabled;
        } hdr{};

        void hdr_init();
        void hdr_resize(int width, int height) const;
        void hdr_start() const;
        void hdr_end() const;
        void hdr_free();

        struct {
            gl::Program *blur;
            std::array<unsigned, 2> FBO;
            std::array<unsigned, 2> colorBuffers;
            bool enabled;
        } bloom{};

        void bloom_init();
        void bloom_resize(int width, int height) const;
        void bloom_free();

        struct {
            gl::Program *shader;
            unsigned depthMap;
            unsigned depthCubemap;
            bool enabled;
        } shadows{};

        void shadows_init();
        void shadows_draw(float delta);
        void shadows_free();

        Camera *active_camera = nullptr;

        void _draw_ui();

       protected:
        explicit Scene(gl::Program *program = nullptr, bool hdr = false, bool bloom = false,
                       bool shadows = false);

        gl::Program *get_program();

        inline static glm::vec3 clear_color;
        virtual void draw_ui() {}
        virtual void update(float delta) {}

       public:
        ~Scene() override;

        void _resize(int width, int height) const;
        void _draw(float delta, gl::Program *shader) override;

        void set_camera(Camera *camera);
        Camera *get_camera();

        virtual std::string scene_name() = 0;
    };
}  // namespace gl

#endif  // SCENE_HPP
