#ifndef SHADER_HPP
#define SHADER_HPP

#include <glm/glm.hpp>
#include <string>
#include <vector>

#include "glad/glad.h"

namespace gl {
    class Shader {
        std::string filename;
        std::string source;
        GLenum type{};
        unsigned int ID = 0;

       public:
        Shader(std::string filename, GLenum type);
        Shader(std::string filename, std::string source, GLenum type);

        void compile();
        [[nodiscard]] unsigned int getID() const;

        ~Shader();
    };

    class Program {
        unsigned int ID;
        inline static Program *_active = nullptr;

       public:
        explicit Program(const std::vector<Shader> &shaders);

        [[nodiscard]] unsigned int getID() const;
        void use();
        static Program *active();

        void setBool(const std::string &name, bool value) const;
        void setInt(const std::string &name, int value) const;
        void setFloat(const std::string &name, float value) const;
        void setVec2(const std::string &name, const glm::vec2 &value) const;
        void setVec2(const std::string &name, float x, float y) const;
        void setVec3(const std::string &name, const glm::vec3 &value) const;
        void setVec3(const std::string &name, float x, float y, float z) const;
        void setVec4(const std::string &name, const glm::vec4 &value) const;
        void setVec4(const std::string &name, float x, float y, float z, float w) const;
        void setMat2(const std::string &name, const glm::mat2 &mat) const;
        void setMat3(const std::string &name, const glm::mat3 &mat) const;
        void setMat4(const std::string &name, const glm::mat4 &mat) const;

        ~Program();
    };
}  // namespace gl

#endif  // SHADER_HPP
