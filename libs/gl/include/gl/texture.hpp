#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include <string>

namespace gl {
    struct Texture {
        unsigned int id;
        std::string type;
        std::string path;
    };
}  // namespace gl

#endif  // TEXTURE_HPP
