#ifndef VERTEX_HPP
#define VERTEX_HPP

#include <glm/glm.hpp>

namespace gl {
    struct Vertex {
        glm::vec3 position;
        glm::vec3 normal;
        glm::vec2 textureCoords;
        glm::vec3 tangents;
        glm::vec3 bitangent;
    };
}  // namespace gl

#endif  // VERTEX_HPP
