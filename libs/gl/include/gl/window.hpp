#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <glad/glad.h>

// include after glad
#include <GLFW/glfw3.h>

#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

namespace gl {
    using click_callback  = void (*)(int key, int scancode, int action, int mods);
    using mouse_callback  = void (*)(double xpos, double ypos);
    using scroll_callback = void (*)(double xoffset, double yoffset);
    using resize_callback = void (*)(int width, int height);
    using press_callback  = void (*)(double delta);

    class Window {
        static std::unordered_map<std::string, click_callback> key_callbacks;
        static std::unordered_map<std::string, press_callback> process_callbacks;
        static std::unordered_map<std::string, mouse_callback> mouse_callbacks;
        static std::unordered_map<std::string, scroll_callback> scroll_callbacks;
        static std::unordered_map<std::string, resize_callback> resize_callbacks;

        double lastFrame = 0.0f;

        GLFWwindow *window       = nullptr;
        inline static int width  = 0;
        inline static int height = 0;

        int frames       = 0;
        double last_time = 0;

       public:
        Window(const std::string &window_title, int _width, int _height);
        ~Window();

        void close() const;
        [[nodiscard]] bool should_close() const;
        [[nodiscard]] static double current_time();
        void set_input_mode(int mode, int value);
        static std::array<int, 2> get_size();
        static float aspect_ratio();

        static void add_click_callback(const std::string &name, click_callback callback);
        static void remove_click_callback(const std::string &name);

        static void add_press_callback(const std::string &name, press_callback callback);
        static void remove_press_callback(const std::string &name);
        bool key_clicked(int key);

        static void add_mouse_callback(const std::string &name, mouse_callback callback);
        static void remove_mouse_callback(const std::string &name);

        static void add_scroll_callback(const std::string &name, scroll_callback callback);
        static void remove_scroll_callback(const std::string &name);

        static void add_resize_callback(const std::string &name, resize_callback callback);
        static void remove_resize_callback(const std::string &name);

        template <typename F>
        void game_loop(const F &loop) {
            while (!should_close()) {
                const auto currentFrame = current_time();
                const auto delta        = currentFrame - lastFrame;
                lastFrame               = currentFrame;

                do_press_callback(window, delta);

                // FPS benchmarking

                //                frames++;
                //                auto time = glfwGetTime();
                //                if (time - last_time >= 1.0) {
                //                    std::cout << static_cast<double>(frames) / (time - last_time)
                //                    << "fps\n"; last_time = time; frames = 0;
                //                }

                loop(delta);

                glfwSwapBuffers(window);
                glfwPollEvents();
            }
        }

       private:
        static void do_key_callback(GLFWwindow *window, int key, int code, int action, int mods);
        static void do_mouse_callback(GLFWwindow *window, double xpos, double ypos);
        static void do_scroll_callback(GLFWwindow *window, double xoffset, double yoffset);
        static void do_press_callback(GLFWwindow *_window, double delta);
        static void do_resize_callback(GLFWwindow *_window, int _width, int _height);
    };
}  // namespace gl

#endif  // WINDOW_HPP
