#include "gl/camera.hpp"

#include <gl/shader.hpp>
#include <gl/window.hpp>

glm::mat4 gl::Camera::getView() const {
    return glm::lookAt(get_position(), get_position() + front(), up());
}

glm::mat4 gl::Camera::getProjection(float aspect_ratio) const {
    return glm::perspective(glm::radians(zoom), aspect_ratio, near, far);
}

float gl::Camera::get_zoom() const { return zoom; }
void gl::Camera::set_zoom(float _zoom) { Camera::zoom = _zoom; }

bool gl::Camera::get_active() const { return active; }
void gl::Camera::set_active(bool _active) { this->active = _active; }

float gl::Camera::get_near() const { return near; }
void gl::Camera::set_near(float _near) { this->near = _near; }

float gl::Camera::get_far() const { return far; }
void gl::Camera::set_far(float _far) { this->far = _far; }

void gl::Camera::draw([[maybe_unused]] float delta, gl::Program *shader) {
    if (active) {
        shader->setVec3("viewPosition", get_position());
        shader->setMat4("projection", getProjection(gl::Window::aspect_ratio()));
        shader->setMat4("view", getView());
    }
}

gl::Camera *gl::Camera::Builder::build() {
    auto *built = new Camera();

    built->set_position(get_position());
    built->set_name(get_name());
    built->set_children(get_children());
    built->set_rotation(get_rotation());
    built->set_scale(get_scale());
    built->set_zoom(get_zoom());
    built->set_active(get_active());
    built->set_near(get_near());
    built->set_far(get_far());

    delete this;
    return built;
}

float gl::PlayerCamera::get_movement_speed() const { return movement_speed; }
void gl::PlayerCamera::set_movement_speed(float _movement_speed) {
    movement_speed = _movement_speed;
}

float gl::PlayerCamera::get_mouse_sensitivity() const { return mouse_sensitivity; }
void gl::PlayerCamera::set_mouse_sensitivity(float _sensitivity) {
    mouse_sensitivity = _sensitivity;
}

void gl::PlayerCamera::process_movement(float xoffset, float yoffset) {
    rotate_around(gl::up, get_position(), -xoffset * mouse_sensitivity);
    rotate_around(right(), get_position(), yoffset * mouse_sensitivity);
}

gl::PlayerCamera *gl::PlayerCamera::Builder::build() {
    auto *built = new PlayerCamera();

    built->set_position(get_position());
    built->set_name(get_name());
    built->set_children(get_children());
    built->set_rotation(get_rotation());
    built->set_scale(get_scale());
    built->set_zoom(get_zoom());
    built->set_active(get_active());
    built->set_near(get_near());
    built->set_far(get_far());
    built->set_mouse_sensitivity(get_mouse_sensitivity());
    built->set_movement_speed(get_movement_speed());

    delete this;
    return built;
}