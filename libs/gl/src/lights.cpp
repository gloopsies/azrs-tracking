#include <gl/lights.hpp>

gl::DirectionalLight::DirectionalLight(bool is_directional) : is_directional(is_directional) {
    if (is_directional) {
        lights_index            = lights_len++;
        lights.at(lights_index) = this;
    }
}

const glm::vec3 &gl::DirectionalLight::get_ambient() const { return ambient; }
void gl::DirectionalLight::set_ambient(const glm::vec3 &_ambient) { ambient = _ambient; }

const glm::vec3 &gl::DirectionalLight::get_diffuse() const { return diffuse; }
void gl::DirectionalLight::set_diffuse(const glm::vec3 &_diffuse) { diffuse = _diffuse; }

const glm::vec3 &gl::DirectionalLight::get_specular() const { return specular; }
void gl::DirectionalLight::set_specular(const glm::vec3 &_specular) { specular = _specular; }

gl::DirectionalLight::~DirectionalLight() {
    if (is_directional) {
        // TODO: delete light from array
    }
}

gl::PointLight::PointLight(bool is_point) : DirectionalLight(false), is_point(is_point) {
    if (is_point) {
        lights_index            = lights_len++;
        lights.at(lights_index) = this;
    }
}

float gl::PointLight::get_constant() const { return constant; }
void gl::PointLight::set_constant(float _constant) { constant = _constant; }

float gl::PointLight::get_linear() const { return linear; }
void gl::PointLight::set_linear(float _linear) { linear = _linear; }

float gl::PointLight::get_quadratic() const { return quadratic; }
void gl::PointLight::set_quadratic(float _quadratic) { quadratic = _quadratic; }

gl::PointLight::~PointLight() {
    if (is_point) {
        // TODO: delete light from array
    }
}

gl::SpotLight::SpotLight(bool is_spot) : PointLight(false), is_spot(is_spot) {
    if (is_spot) {
        lights_index            = lights_len++;
        lights.at(lights_index) = this;
    }
}

float gl::SpotLight::get_inner_cutoff() const { return inner_cutoff; }
void gl::SpotLight::set_inner_cutoff(float _inner_cutoff) { inner_cutoff = _inner_cutoff; }

float gl::SpotLight::get_outer_cutoff() const { return outer_cutoff; }
void gl::SpotLight::set_outer_cutoff(float _outer_cutoff) { outer_cutoff = _outer_cutoff; }

gl::SpotLight::~SpotLight() {
    if (is_spot) {
        // TODO: delete light from array
    }
}

gl::DirectionalLight *gl::DirectionalLight::Builder::build() {
    auto *built = new DirectionalLight(true);

    built->set_position(get_position());
    built->set_name(get_name());
    built->set_children(get_children());
    built->set_rotation(get_rotation());
    built->set_scale(get_scale());

    built->set_ambient(get_ambient());
    built->set_diffuse(get_diffuse());
    built->set_specular(get_specular());

    delete this;
    return built;
}

gl::PointLight *gl::PointLight::Builder::build() {
    auto *built = new PointLight(true);

    built->set_position(get_position());
    built->set_name(get_name());
    built->set_children(get_children());
    built->set_rotation(get_rotation());
    built->set_scale(get_scale());

    built->set_ambient(get_ambient());
    built->set_diffuse(get_diffuse());
    built->set_specular(get_specular());

    built->set_constant(get_constant());
    built->set_linear(get_linear());
    built->set_quadratic(get_quadratic());

    delete this;
    return built;
}

gl::SpotLight *gl::SpotLight::Builder::build() {
    auto *built = new SpotLight(true);

    built->set_position(get_position());
    built->set_name(get_name());
    built->set_children(get_children());
    built->set_rotation(get_rotation());
    built->set_scale(get_scale());

    built->set_ambient(get_ambient());
    built->set_diffuse(get_diffuse());
    built->set_specular(get_specular());

    built->set_constant(get_constant());
    built->set_linear(get_linear());
    built->set_quadratic(get_quadratic());

    built->set_inner_cutoff(get_inner_cutoff());
    built->set_outer_cutoff(get_outer_cutoff());

    delete this;
    return built;
}
