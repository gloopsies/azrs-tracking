#include <gl/mesh_instance.hpp>

gl::Model *gl::MeshInstance::get_model() const { return model; }
void gl::MeshInstance::set_model(gl::Model *_model) { model = _model; }
void gl::MeshInstance::_draw(float delta, gl::Program *shader) {
    if (model != nullptr && shader != nullptr) {
        shader->use();
        shader->setMat4("model", get_world_transform() *
                                     glm::translate(glm::mat4(1), -model->get_reference_point()));
        model->draw(*shader);
    }

    for (auto *child : get_children()) {
        child->_draw(delta, shader);
    }
}

gl::MeshInstance::~MeshInstance() { delete model; }

gl::MeshInstance *gl::MeshInstance::Builder::build() {
    auto *built = new MeshInstance();

    built->set_position(get_position());
    built->set_name(get_name());
    built->set_children(get_children());
    built->set_rotation(get_rotation());
    built->set_scale(get_scale());
    built->set_model(get_model());

    delete this;
    return built;
}