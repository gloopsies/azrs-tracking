#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <stb/image.h>

#include <assimp/Importer.hpp>
#include <gl/model.hpp>
#include <stdexcept>

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

unsigned int textureFromFile(const std::string& file);

gl::Model::Model(const std::string& path, enum POSITION x, enum POSITION y, enum POSITION z) {
    load(path);

    switch (x) {
        case DEFAULT:
            reference_point.x = 0;
            break;
        case BACK:
            reference_point.x = bounding_box[0].x;
            break;
        case FRONT:
            reference_point.x = bounding_box[1].x;
            break;
        case CENTER:
            reference_point.x = (bounding_box[0].x + bounding_box[1].x) / 2;
            break;
    }

    switch (y) {
        case DEFAULT:
            reference_point.x = 0;
            break;
        case BACK:
            reference_point.y = bounding_box[0].y;
            break;
        case FRONT:
            reference_point.y = bounding_box[1].y;
            break;
        case CENTER:
            reference_point.y = (bounding_box[0].y + bounding_box[1].y) / 2;
            break;
    }

    switch (z) {
        case DEFAULT:
            reference_point.z = 0;
            break;
        case BACK:
            reference_point.z = bounding_box[0].z;
            break;
        case FRONT:
            reference_point.z = bounding_box[1].z;
            break;
        case CENTER:
            reference_point.z = (bounding_box[0].z + bounding_box[1].z) / 2;
            break;
    }
}

void gl::Model::draw(const Program& shader) const {
    for (const Mesh& mesh : meshes) {
        mesh.draw(shader);
    }
}

void gl::Model::load(const std::string& path) {
    Assimp::Importer importer;

    const aiScene* scene = importer.ReadFile(
        path, aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_CalcTangentSpace);

    if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
        throw std::runtime_error("Failed loading model " + path + ": " + importer.GetErrorString());
    }

    this->directory = path.substr(0, path.find_last_of('/'));
    processNode(scene->mRootNode, scene);
}

void gl::Model::processNode(const aiNode* node, const aiScene* scene) {
    for (unsigned int i = 0; i < node->mNumMeshes; ++i) {
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        meshes.push_back(processMesh(mesh, scene));
    }

    for (unsigned int i = 0; i < node->mNumChildren; ++i) {
        processNode(node->mChildren[i], scene);
    }
}

gl::Mesh gl::Model::processMesh(aiMesh* mesh, const aiScene* scene) {
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<Texture> textures;

    bounding_box[0] = {mesh->mVertices[0].x, mesh->mVertices[0].y, mesh->mVertices[0].z};
    bounding_box[1] = bounding_box[0];

    for (unsigned int i = 0; i < mesh->mNumVertices; ++i) {
        Vertex vertex{};

        vertex.position.x = mesh->mVertices[i].x;
        vertex.position.y = mesh->mVertices[i].y;
        vertex.position.z = mesh->mVertices[i].z;

        bounding_box[0] = {
            MIN(bounding_box[0].x, vertex.position.x),
            MIN(bounding_box[0].y, vertex.position.y),
            MAX(bounding_box[0].z, vertex.position.z),
        };

        bounding_box[1] = {
            MAX(bounding_box[1].x, vertex.position.x),
            MAX(bounding_box[1].y, vertex.position.y),
            MIN(bounding_box[1].z, vertex.position.z),
        };

        if (mesh->HasNormals()) {
            vertex.normal.x = mesh->mNormals[i].x;
            vertex.normal.y = mesh->mNormals[i].y;
            vertex.normal.z = mesh->mNormals[i].z;
        }

        if (mesh->mTextureCoords[0]) {
            vertex.textureCoords.x = mesh->mTextureCoords[0][i].x;
            vertex.textureCoords.y = mesh->mTextureCoords[0][i].y;

            vertex.tangents.x = mesh->mTangents[i].x;
            vertex.tangents.y = mesh->mTangents[i].y;
            vertex.tangents.z = mesh->mTangents[i].z;

            vertex.bitangent.x = mesh->mBitangents[i].x;
            vertex.bitangent.y = mesh->mBitangents[i].y;
            vertex.bitangent.z = mesh->mBitangents[i].z;
        } else {
            vertex.textureCoords = glm::vec2(0.0);
        }

        vertices.push_back(vertex);
    }

    for (unsigned i = 0; i < mesh->mNumFaces; ++i) {
        const aiFace face = mesh->mFaces[i];

        for (unsigned j = 0; j < face.mNumIndices; ++j) {
            indices.push_back(face.mIndices[j]);
        }
    }

    aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

    loadTextureMaterial(material, aiTextureType_DIFFUSE, "texture_diffuse", textures);
    loadTextureMaterial(material, aiTextureType_SPECULAR, "texture_specular", textures);
    loadTextureMaterial(material, aiTextureType_NORMALS, "texture_normal", textures);
    loadTextureMaterial(material, aiTextureType_HEIGHT, "texture_height", textures);

    return {vertices, indices, textures};
}

void gl::Model::loadTextureMaterial(const aiMaterial* mat, const aiTextureType type,
                                    const std::string& typeName, std::vector<Texture>& textures) {
    for (unsigned int i = 0; i < mat->GetTextureCount(type); ++i) {
        aiString str;
        mat->GetTexture(type, i, &str);

        bool skip = false;

        for (auto& loaded_texture : loaded_textures) {
            if (std::strcmp(str.C_Str(), loaded_texture.path.c_str()) == 0) {
                textures.push_back(loaded_texture);
                skip = true;
                break;
            }
        }

        if (!skip) {
            Texture texture;
            const std::string file_name = this->directory + "/" + str.C_Str();
            texture.id                  = textureFromFile(file_name);
            texture.type                = typeName;
            texture.path                = str.C_Str();
            textures.push_back(texture);
            loaded_textures.push_back(texture);
        }
    }
}

void gl::Model::set_shader_prefix(const std::string& prefix) {
    for (Mesh& mesh : meshes) {
        mesh.set_identifier_prefix(prefix);
    }
}

glm::vec3 gl::Model::get_reference_point() const { return reference_point; }
void gl::Model::set_reference_point(glm::vec3 _reference_point) {
    reference_point = _reference_point;
}

unsigned int textureFromFile(const std::string& file) {
    unsigned int textureID = 0;
    glGenTextures(1, &textureID);

    int width        = 0;
    int height       = 0;
    int nrComponents = 0;

    stbi_set_flip_vertically_on_load(true);
    unsigned char* data = stbi_load(file.c_str(), &width, &height, &nrComponents, 0);
    if (data) {
        GLint format0 = 0;
        GLint format  = 0;
        switch (nrComponents) {
            case 1:
                format0 = GL_RED;
                format  = GL_RED;
                break;
            case 2:
                format0 = GL_RG;
                format  = GL_RG;
                break;
            case 3:
                format0 = GL_SRGB;
                format  = GL_RGB;
                break;
            case 4:
                format0 = GL_SRGB_ALPHA;
                format  = GL_RGBA;
                break;
            default:
                throw std::runtime_error("Unknown format");
        }

        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, format0, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    } else {
        throw std::runtime_error("Failed to load texture image: " + file);
    }

    stbi_image_free(data);
    return textureID;
}

gl::Model* gl::Model::Builder::build() {
    auto* built = new Model(get_path(), $x, $y, $z);

    built->set_shader_prefix(get_shader_prefix());

    delete this;
    return built;
}
gl::Model::Builder* gl::Model::Builder::reference_point(enum POSITION _x, enum POSITION _y,
                                                        enum POSITION _z) {
    $x = _x;
    $y = _y;
    $z = _z;

    return this;
}
gl::Model::Builder* gl::Model::Builder::reference_point(enum POSITION position) {
    $x = $y = $z = position;

    return this;
}
