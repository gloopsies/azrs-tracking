#include "gl/node.hpp"

#include <glm/gtx/matrix_decompose.hpp>
#include <iostream>
#include <utility>

#include "gl/constants.hpp"

void gl::Node::set_name(std::string _name) { name = std::move(_name); }

std::vector<gl::Node *> gl::Node::get_children() { return children; }
void gl::Node::add_child(Node *_child) {
    children.push_back(_child);
    _child->parent = this;
}

void gl::Node::add_children(const std::vector<Node *> &_children) {
    auto _size = children.size();
    children.insert(children.end(), _children.begin(), _children.end());

    for (auto i = _size; i < children.size(); ++i) {
        children[i]->parent = this;
    }
}
void gl::Node::set_children(std::vector<Node *> _children) {
    children = std::move(_children);

    for (auto &i : children) {
        i->parent = this;
    }
}

glm::vec3 gl::Node::get_position() const { return position; }
glm::vec3 gl::Node::get_world_position() const {
    return get_world_transform() * glm::vec4(glm::vec3(0), 1);
}

void gl::Node::set_position(const glm::vec3 &_position) { position = _position; }
void gl::Node::move(glm::vec3 move_by) { position += move_by; }

const glm::quat &gl::Node::get_rotation() const { return rotation; }
void gl::Node::set_rotation(const glm::quat &_rotation) { Node::rotation = _rotation; }
void gl::Node::rotate(glm::vec3 _rotation) {
    rotation = glm::rotate(rotation, glm::radians(_rotation[0]), glm::vec3(1, 0, 0));
    rotation = glm::rotate(rotation, glm::radians(_rotation[1]), glm::vec3(0, 1, 0));
    rotation = glm::rotate(rotation, glm::radians(_rotation[2]), glm::vec3(0, 0, 1));
}

void gl::Node::rotate_around(glm::vec3 vector, float angle) {
    auto transform =
        glm::rotate(glm::mat4(1), glm::radians(angle), normalize(vector)) * get_transform();

    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(transform, scale, rotation, position, skew, perspective);
}

void gl::Node::rotate_around(glm::vec3 vector, glm::vec3 point, float angle) {
    vector = normalize(vector);
    angle  = glm::radians(angle);

    auto transform = glm::mat4(1);
    transform      = get_transform() * transform;
    transform      = glm::translate(glm::mat4(1), -point) * transform;
    transform      = glm::rotate(glm::mat4(1), angle, vector) * transform;
    transform      = glm::translate(glm::mat4(1), point) * transform;

    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(transform, scale, rotation, position, skew, perspective);
}

void gl::Node::lookAt(gl::Node &other) {
    (void)other;
    (void)position;
    // TODO: implement Node::lookAt
}

const glm::vec3 &gl::Node::get_scale() const { return scale; }
void gl::Node::set_scale(const glm::vec3 &_scale) { scale = _scale; }

glm::vec3 gl::Node::front() const { return rotation * gl::front; }
glm::vec3 gl::Node::right() const { return rotation * gl::right; }
glm::vec3 gl::Node::up() const { return rotation * gl::up; }

gl::Node::~Node() {
    for (auto *child : children) {
        delete child;
    }
}

// TODO: better tree printing
void gl::Node::print(const std::string &prefix, int last) {
    (void)last;

    std::cout << prefix << get_node_type() << "\n";
    for (auto *child : this->children) {
        child->print(prefix + "\t");
    }
}

gl::Node *gl::Node::get(const std::string &_name) {
    auto index = _name.find('/');

    if (index == std::string::npos) {
        for (auto *child : children) {
            if (child->name == _name) {
                return child;
            }
        }

        throw std::runtime_error("No Node named " + _name);
    }

    auto expected = _name.substr(0, index);
    for (auto *child : children) {
        if (child->name == expected) {
            return child->get(_name.substr(index + 1));
        }
    }

    throw std::runtime_error("No Node named " + _name);
}

glm::mat4 gl::Node::get_transform() const {
    auto transformation = glm::mat4(1);

    transformation = glm::toMat4(rotation) * transformation;
    transformation = glm::scale(glm::mat4(1), scale) * transformation;
    transformation = glm::translate(glm::mat4(1), position) * transformation;

    return transformation;
}

glm::mat4 gl::Node::get_world_transform() const {
    auto parent_transform = (parent == nullptr) ? glm::mat4(1) : parent->get_world_transform();

    return parent_transform * get_transform();
}

void gl::Node::_key_clicked(int key, int scancode, int action, int mods) {
    key_clicked(key, scancode, action, mods);

    for (auto *child : children) {
        child->_key_clicked(key, scancode, action, mods);
    }
}

void gl::Node::_key_pressed(double delta) {
    key_pressed(delta);

    for (auto *child : children) {
        child->_key_pressed(delta);
    }
}

void gl::Node::_mouse_moved(double xpos, double ypos) {
    mouse_moved(xpos, ypos);

    for (auto *child : children) {
        child->_mouse_moved(xpos, ypos);
    }
}

void gl::Node::_draw(float delta, gl::Program *shader) {
    draw(delta, shader);

    for (auto *child : children) {
        child->_draw(delta, shader);
    }
}

gl::Node *gl::Node::Builder::build() {
    auto *built     = new Node();
    built->position = $position;
    built->name     = std::move($name);
    built->children = std::move($children);
    built->rotation = $rotation;
    built->scale    = $scale;

    delete this;
    return built;
}
