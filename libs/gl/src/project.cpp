#include "gl/project.hpp"

#include <utility>

std::string gl::Project::name;
gl::Window* gl::Project::window      = nullptr;
gl::Scene* gl::Project::active_scene = nullptr;

void gl::Project::init(std::string _name, int width, int height) {
    name   = std::move(_name);
    window = new gl::Window(name, width, height);
}

gl::Window* gl::Project::get_window() { return window; }

void gl::Project::set_scene(gl::Scene* scene) {
    gl::Window::remove_mouse_callback("project::scene");
    gl::Window::remove_click_callback("project::scene");
    gl::Window::remove_press_callback("project::scene");
    gl::Window::remove_resize_callback("project::scene");

    delete active_scene;
    active_scene = scene;

    gl::Window::add_mouse_callback("project::scene", [](const double xpos, const double ypos) {
        active_scene->_mouse_moved(xpos, ypos);
    });

    gl::Window::add_click_callback("project::scene",
                                   [](int key, int scancode, int action, int mods) {
                                       active_scene->_key_clicked(key, scancode, action, mods);
                                   });

    gl::Window::add_press_callback("project::scene",
                                   [](double delta) { active_scene->_key_pressed(delta); });

    gl::Window::add_resize_callback(
        "project::scene", [](int width, int height) { active_scene->_resize(width, height); });
}

void gl::Project::run() {
    window->game_loop([](float delta) {
        if (active_scene != nullptr) {
            active_scene->_draw(delta, nullptr);
        }
    });
}

bool gl::Project::key_clicked(int key) { return window->key_clicked(key); }

void gl::Project::close() { window->close(); }

void gl::Project::finalize() {
    delete window;
    delete active_scene;
}
