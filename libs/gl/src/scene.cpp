#include "gl/scene.hpp"

constexpr unsigned BLUR_AMOUNT   = 10;
constexpr unsigned SHADOW_WIDTH  = 1024;
constexpr unsigned SHADOW_HEIGHT = 1024;

gl::Scene::Scene(gl::Program *program, bool hdr, bool bloom, bool shadows)
    : shader_program(program) {
    this->hdr.enabled = hdr;
    hdr_init();

    this->bloom.enabled = bloom;
    bloom_init();

    this->shadows.enabled = shadows;
    shadows_init();
}

void gl::Scene::set_camera(Camera *camera) { active_camera = camera; }
gl::Camera *gl::Scene::get_camera() { return active_camera; }

gl::Program *gl::Scene::get_program() { return shader_program; }

void gl::Scene::hdr_init() {
    if (!hdr.enabled) {
        return;
    }

    auto shaders = std::vector<gl::Shader>();
    shaders.reserve(2);
    shaders.emplace_back(DATADIR "/shaders/screen.vert", GL_VERTEX_SHADER);
    shaders.emplace_back(DATADIR "/shaders/hdr.frag", GL_FRAGMENT_SHADER);
    hdr.shader = new gl::Program(shaders);

    auto window_size = gl::Window::get_size();
    glGenFramebuffers(1, &hdr.FBO);
    glBindFramebuffer(GL_FRAMEBUFFER, hdr.FBO);

    glGenTextures(2, hdr.colorBuffers.data());
    for (unsigned int i = 0; i < 2; i++) {
        glBindTexture(GL_TEXTURE_2D, hdr.colorBuffers.at(i));
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, window_size[0], window_size[1], 0, GL_RGBA,
                     GL_FLOAT, nullptr);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D,
                               hdr.colorBuffers.at(i), 0);
    }

    glGenRenderbuffers(1, &hdr.depthBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, hdr.depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, window_size[0], window_size[1]);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER,
                              hdr.depthBuffer);

    std::array<unsigned, 2> attachments = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
    glDrawBuffers(2, attachments.data());
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        throw std::runtime_error("HDR Framebuffer not complete!");
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    hdr.shader->use();
    hdr.shader->setFloat("exposure", hdr.exposure);
}

void gl::Scene::bloom_init() {
    if (!this->bloom.enabled) {
        return;
    }

    auto window_size = gl::Window::get_size();

    glGenFramebuffers(2, bloom.FBO.data());
    glGenTextures(2, bloom.colorBuffers.data());
    for (unsigned int i = 0; i < 2; i++) {
        glBindFramebuffer(GL_FRAMEBUFFER, bloom.FBO.at(i));
        glBindTexture(GL_TEXTURE_2D, bloom.colorBuffers.at(i));
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, window_size[0], window_size[1], 0, GL_RGBA,
                     GL_FLOAT, nullptr);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                               bloom.colorBuffers.at(i), 0);

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            throw std::runtime_error("Bloom Framebuffer not complete!");
        }
    }

    auto shaders = std::vector<gl::Shader>();
    shaders.reserve(2);
    shaders.emplace_back(DATADIR "/shaders/screen.vert", GL_VERTEX_SHADER);
    shaders.emplace_back(DATADIR "/shaders/blur.frag", GL_FRAGMENT_SHADER);
    bloom.blur = new gl::Program(shaders);
}

void gl::Scene::shadows_init() {
    if (!this->shadows.enabled) {
        return;
    }

    auto shaders = std::vector<gl::Shader>();
    shaders.reserve(3);
    shaders.emplace_back(DATADIR "/shaders/shadow.vert", GL_VERTEX_SHADER);
    shaders.emplace_back(DATADIR "/shaders/shadow.geom", GL_GEOMETRY_SHADER);
    shaders.emplace_back(DATADIR "/shaders/shadow.frag", GL_FRAGMENT_SHADER);
    shadows.shader = new gl::Program(shaders);

    glGenFramebuffers(1, &shadows.depthMap);

    glGenTextures(1, &shadows.depthCubemap);
    glBindTexture(GL_TEXTURE_CUBE_MAP, shadows.depthCubemap);

    for (unsigned int i = 0; i < 6; ++i) {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH,
                     SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    glBindFramebuffer(GL_FRAMEBUFFER, shadows.depthMap);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, shadows.depthCubemap, 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    shader_program->use();
    shader_program->setInt("depthMap", 1);
}

void gl::Scene::hdr_resize(int width, int height) const {
    if (!this->hdr.enabled) {
        return;
    }

    for (unsigned int i = 0; i < 2; i++) {
        glBindTexture(GL_TEXTURE_2D, hdr.colorBuffers.at(i));
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, nullptr);
    }

    glBindRenderbuffer(GL_RENDERBUFFER, hdr.depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
}

void gl::Scene::bloom_resize(int width, int height) const {
    if (!this->bloom.enabled) {
        return;
    }

    for (unsigned int i = 0; i < 2; i++) {
        glBindTexture(GL_TEXTURE_2D, bloom.colorBuffers.at(i));
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, nullptr);
    }
}

void gl::Scene::_resize(int width, int height) const {
    hdr_resize(width, height);
    bloom_resize(width, height);
}

void gl::Scene::hdr_start() const {
    if (!hdr.enabled) {
        return;
    }

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glBindFramebuffer(GL_FRAMEBUFFER, hdr.FBO);
}

void gl::Scene::hdr_end() const {
    if (!this->hdr.enabled) {
        return;
    }

    bool horizontal = true;
    if (bloom.enabled) {
        bloom.blur->use();

        for (unsigned int i = 0; i < BLUR_AMOUNT; i++) {
            glBindFramebuffer(GL_FRAMEBUFFER, bloom.FBO.at(horizontal));
            bloom.blur->setInt("horizontal", horizontal);
            glBindTexture(GL_TEXTURE_2D,
                          i == 0 ? hdr.colorBuffers[1] : bloom.colorBuffers.at(!horizontal));

            gl::quad::render();

            horizontal = !horizontal;
        }
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    hdr.shader->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, hdr.colorBuffers[0]);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, bloom.colorBuffers.at(!horizontal));
    hdr.shader->setFloat("exposure", hdr.exposure);
    hdr.shader->setBool("bloom", bloom.enabled);

    gl::quad::render();
}

void gl::Scene::shadows_draw(float delta) {
    if (!this->shadows.enabled) {
        return;
    }

    glClearColor(clear_color.r, clear_color.g, clear_color.b, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    auto lightPos = get("table/lamp/light")->get_world_position();

    auto near = 0.1f;
    auto far  = 75.0f;

    const glm::mat4 shadowProj = glm::perspective(
        glm::radians(90.0f), SHADOW_WIDTH / static_cast<float>(SHADOW_HEIGHT), near, far);

    std::vector<glm::mat4> shadowTransforms;
    shadowTransforms.reserve(6);

    auto directions = std::array<glm::vec3, 6>{
        glm::vec3(1.0f, 0.0f, 0.0f),  glm::vec3(-1.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f),
        glm::vec3(0.0f, -1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f),  glm::vec3(0.0f, 0.0f, -1.0f),
    };

    auto ups = std::array<glm::vec3, 6>{
        glm::vec3(0.0f, -1.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f),
        glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, -1.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f),
    };

    for (size_t i = 0; i < directions.size(); ++i) {
        shadowTransforms.push_back(shadowProj *
                                   glm::lookAt(lightPos, lightPos + directions.at(i), ups.at(i)));
    }

    glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
    glBindFramebuffer(GL_FRAMEBUFFER, shadows.depthMap);
    glClear(GL_DEPTH_BUFFER_BIT);
    shadows.shader->use();
    for (unsigned int i = 0; i < 6; ++i) {
        shadows.shader->setMat4("shadowMatrices[" + std::to_string(i) + "]", shadowTransforms[i]);
    }

    shadows.shader->setVec3("lightPos", lightPos);
    shadows.shader->setFloat("far_plane", far);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_CUBE_MAP, shadows.depthCubemap);
    draw(delta, shadows.shader);
    for (auto *child : get_children()) {
        child->_draw(delta, shadows.shader);
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void gl::Scene::_draw(float delta, [[maybe_unused]] gl::Program *shader) {
    update(delta);

    glDisable(GL_BLEND);
    glDisable(GL_FRAMEBUFFER_SRGB);
    glDisable(GL_CULL_FACE);

    shadows_draw(delta);

    hdr_start();

    glEnable(GL_BLEND);
    glEnable(GL_FRAMEBUFFER_SRGB);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    auto screen_size = gl::Window::get_size();
    glClearColor(clear_color.r, clear_color.g, clear_color.b, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, screen_size[0], screen_size[1]);

    shader_program->use();
    auto far = 75.0f;
    shader_program->setFloat("far_plane", far);
    draw(delta, shader_program);
    for (auto *child : get_children()) {
        child->_draw(delta, shader_program);
    }

    hdr_end();

    _draw_ui();
}

void gl::Scene::_draw_ui() {
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    draw_ui();

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void gl::Scene::hdr_free() {
    if (!this->hdr.enabled) {
        return;
    }

    delete hdr.shader;

    glDeleteRenderbuffers(1, &hdr.depthBuffer);
    glDeleteTextures(2, hdr.colorBuffers.data());
    glDeleteFramebuffers(1, &hdr.FBO);
}

void gl::Scene::bloom_free() {
    if (!this->bloom.enabled) {
        return;
    }

    delete bloom.blur;

    glDeleteTextures(2, bloom.colorBuffers.data());
    glDeleteFramebuffers(2, bloom.FBO.data());
}

void gl::Scene::shadows_free() {
    if (!this->shadows.enabled) {
        return;
    }

    delete shadows.shader;

    glDeleteTextures(1, &shadows.depthCubemap);
    glDeleteFramebuffers(1, &shadows.depthMap);
}

gl::Scene::~Scene() {
    shadows_free();
    bloom_free();
    hdr_free();
}
