#include <fstream>
#include <gl/shader.hpp>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

constexpr int ERROR_LENGTH = 512;

gl::Shader::Shader(std::string filename, const GLenum type)
    : filename(std::move(filename)), type(type) {
    const std::ifstream f(this->filename);
    if (!f) {
        throw std::runtime_error("Can't open file: " + this->filename);
    }

    std::stringstream source_stream;
    source_stream << f.rdbuf();
    source = source_stream.str();

    compile();
}

gl::Shader::Shader(std::string filename, std::string source, const GLenum type)
    : filename(std::move(filename)), source(std::move(source)), type(type) {
    compile();
}

void gl::Shader::compile() {
    ID = glCreateShader(type);

    const char *src = source.c_str();

    glShaderSource(ID, 1, &src, nullptr);
    glCompileShader(ID);

    int success = 0;
    glGetShaderiv(ID, GL_COMPILE_STATUS, &success);
    if (success == 0) {
        std::string info;
        info.resize(ERROR_LENGTH);

        glGetShaderInfoLog(ID, static_cast<int>(info.size()), nullptr, info.data());

        throw std::runtime_error("Error compiling shader `" + filename + "`: " + info);
    }
}

gl::Shader::~Shader() { glDeleteShader(ID); }

unsigned int gl::Shader::getID() const { return ID; }

gl::Program::Program(const std::vector<Shader> &shaders) : ID(glCreateProgram()) {
    if (shaders.empty()) {
        throw std::runtime_error("Error linking shader program: No shaders were passed");
    }

    for (const auto &shader : shaders) {
        glAttachShader(ID, shader.getID());
    }

    glLinkProgram(ID);

    int success = 0;
    glGetProgramiv(ID, GL_LINK_STATUS, &success);
    if (success == 0) {
        std::string info;
        info.resize(ERROR_LENGTH);

        glGetProgramInfoLog(ID, static_cast<int>(info.size()), nullptr, info.data());

        throw std::runtime_error("Error linking shader program: " + info);
    }
}

unsigned int gl::Program::getID() const { return ID; }
void gl::Program::use() {
    _active = this;
    glUseProgram(getID());
}

gl::Program *gl::Program::active() { return _active; }

void gl::Program::setBool(const std::string &name, const bool value) const {
    glUniform1i(glGetUniformLocation(ID, name.c_str()), static_cast<int>(value));
}

void gl::Program::setInt(const std::string &name, const int value) const {
    glUniform1i(glGetUniformLocation(ID, name.c_str()), value);
}

void gl::Program::setFloat(const std::string &name, const float value) const {
    glUniform1f(glGetUniformLocation(ID, name.c_str()), value);
}

void gl::Program::setVec2(const std::string &name, const glm::vec2 &value) const {
    glUniform2fv(glGetUniformLocation(ID, name.c_str()), 1, &value[0]);
}

void gl::Program::setVec2(const std::string &name, float x, float y) const {
    glUniform2f(glGetUniformLocation(ID, name.c_str()), x, y);
}

void gl::Program::setVec3(const std::string &name, const glm::vec3 &value) const {
    glUniform3fv(glGetUniformLocation(ID, name.c_str()), 1, &value[0]);
}

void gl::Program::setVec3(const std::string &name, float x, float y, float z) const {
    glUniform3f(glGetUniformLocation(ID, name.c_str()), x, y, z);
}

void gl::Program::setVec4(const std::string &name, const glm::vec4 &value) const {
    glUniform4fv(glGetUniformLocation(ID, name.c_str()), 1, &value[0]);
}

void gl::Program::setVec4(const std::string &name, float x, float y, float z, float w) const {
    glUniform4f(glGetUniformLocation(ID, name.c_str()), x, y, z, w);
}

void gl::Program::setMat2(const std::string &name, const glm::mat2 &mat) const {
    glUniformMatrix2fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

void gl::Program::setMat3(const std::string &name, const glm::mat3 &mat) const {
    glUniformMatrix3fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

void gl::Program::setMat4(const std::string &name, const glm::mat4 &mat) const {
    glUniformMatrix4fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

gl::Program::~Program() { glDeleteProgram(ID); }
