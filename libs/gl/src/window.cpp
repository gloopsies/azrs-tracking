#include <imgui.h>

#include <array>
#include <gl/window.hpp>
#include <stdexcept>

#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

std::unordered_map<std::string, gl::click_callback> gl::Window::key_callbacks;
std::unordered_map<std::string, gl::press_callback> gl::Window::process_callbacks;
std::unordered_map<std::string, gl::mouse_callback> gl::Window::mouse_callbacks;
std::unordered_map<std::string, gl::scroll_callback> gl::Window::scroll_callbacks;
std::unordered_map<std::string, gl::resize_callback> gl::Window::resize_callbacks;

gl::Window::Window(const std::string &window_title, const int _width, const int _height) {
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_SAMPLES, 4);

#ifdef GLFW_PLATFORM_WAYLAND
    glfwWindowHintString(GLFW_WAYLAND_APP_ID, window_title.c_str());
#endif

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    gl::Window::width  = _width;
    gl::Window::height = _height;
    window             = glfwCreateWindow(_width, _height, window_title.c_str(), nullptr, nullptr);
    if (window == nullptr) {
        glfwTerminate();
        throw std::runtime_error("Failed to create a window");
    }

    glfwMakeContextCurrent(window);
    //    glfwSwapInterval(0);

    if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress))) {
        glfwTerminate();
        throw std::runtime_error("Failed to init GLAD");
    }

    glEnable(GL_MULTISAMPLE);
    glEnable(GL_DEPTH_TEST);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

    glfwSetFramebufferSizeCallback(window, Window::do_resize_callback);
    glfwSetCursorPosCallback(window, Window::do_mouse_callback);
    glfwSetScrollCallback(window, Window::do_scroll_callback);
    glfwSetKeyCallback(window, Window::do_key_callback);

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 330 core");
}

gl::Window::~Window() {
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwTerminate();
}

void gl::Window::close() const { glfwSetWindowShouldClose(window, true); }

bool gl::Window::should_close() const { return glfwWindowShouldClose(window); }

double gl::Window::current_time() { return glfwGetTime(); }

void gl::Window::set_input_mode(int mode, int value) { glfwSetInputMode(window, mode, value); }

void gl::Window::add_click_callback(const std::string &name, click_callback callback) {
    Window::key_callbacks[name] = callback;
}

void gl::Window::remove_click_callback(const std::string &name) {
    Window::key_callbacks.erase(name);
}

void gl::Window::add_press_callback(const std::string &name, press_callback callback) {
    Window::process_callbacks[name] = callback;
}

void gl::Window::remove_press_callback(const std::string &name) {
    Window::process_callbacks.erase(name);
}
bool gl::Window::key_clicked(int key) { return glfwGetKey(window, key); }

void gl::Window::add_mouse_callback(const std::string &name, gl::mouse_callback callback) {
    Window::mouse_callbacks[name] = callback;
}

void gl::Window::remove_mouse_callback(const std::string &name) {
    Window::mouse_callbacks.erase(name);
}

void gl::Window::add_scroll_callback(const std::string &name, gl::scroll_callback callback) {
    Window::scroll_callbacks[name] = callback;
}

void gl::Window::remove_scroll_callback(const std::string &name) {
    Window::scroll_callbacks.erase(name);
}

void gl::Window::add_resize_callback(const std::string &name, gl::resize_callback callback) {
    Window::resize_callbacks[name] = callback;
}

void gl::Window::remove_resize_callback(const std::string &name) {
    Window::resize_callbacks.erase(name);
}

void gl::Window::do_key_callback([[maybe_unused]] GLFWwindow *_window, const int key, int scancode,
                                 const int action, int mods) {
    for (const auto &[_, callback] : Window::key_callbacks) {
        callback(key, scancode, action, mods);
    }
}

void gl::Window::do_press_callback([[maybe_unused]] GLFWwindow *_window, double delta) {
    for (const auto &[_, callback] : Window::process_callbacks) {
        callback(delta);
    }
}

void gl::Window::do_mouse_callback([[maybe_unused]] GLFWwindow *_window, double xpos, double ypos) {
    for (const auto &[_, callback] : Window::mouse_callbacks) {
        callback(xpos, ypos);
    }
}

void gl::Window::do_scroll_callback([[maybe_unused]] GLFWwindow *_window, double xoffset,
                                    double yoffset) {
    for (const auto &[_, callback] : Window::scroll_callbacks) {
        callback(xoffset, yoffset);
    }
}

void gl::Window::do_resize_callback([[maybe_unused]] GLFWwindow *_window, int _width, int _height) {
    glViewport(0, 0, _width, _height);
    gl::Window::width  = _width;
    gl::Window::height = _height;
    for (const auto &callback : Window::resize_callbacks) {
        callback.second(_width, _height);
    }
}

std::array<int, 2> gl::Window::get_size() { return {width, height}; }

float gl::Window::aspect_ratio() { return static_cast<float>(width) / static_cast<float>(height); }
