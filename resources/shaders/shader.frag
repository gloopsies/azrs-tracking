#version 330 core
layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BrightColor;

struct PointLight {
   vec3 position;

   vec3 specular;
   vec3 diffuse;
   vec3 ambient;

   float constant;
   float linear;
   float quadratic;
};

struct Material {
   sampler2D texture_diffuse1;
   sampler2D texture_specular1;

   float shininess;
};

in vec2 TexCoords;
in vec3 Normal;
in vec3 FragPos;

uniform PointLight pointLight;
uniform Material material;

uniform samplerCube depthMap;
uniform float far_plane;
uniform vec3 viewPosition;


vec3 gridSamplingDisk[20] = vec3[] (
   vec3(1, 1,  1), vec3( 1, -1,  1), vec3(-1, -1,  1), vec3(-1, 1,  1),
   vec3(1, 1, -1), vec3( 1, -1, -1), vec3(-1, -1, -1), vec3(-1, 1, -1),
   vec3(1, 1,  0), vec3( 1, -1,  0), vec3(-1, -1,  0), vec3(-1, 1,  0),
   vec3(1, 0,  1), vec3(-1,  0,  1), vec3( 1,  0, -1), vec3(-1, 0, -1),
   vec3(0, 1,  1), vec3( 0, -1,  1), vec3( 0, -1, -1), vec3( 0, 1, -1)
);


float ShadowCalculation(vec3 fragPos) {
   vec3 fragToLight = fragPos - pointLight.position;
   float currentDepth = length(fragToLight);
   float bias = 0;
   float shadow = 0.0;
   int samples = 20;
   float viewDistance = length(viewPosition - fragPos);
   float diskRadius = (1.0 + (viewDistance / far_plane)) / 25.0;

   for(int i = 0; i < samples; ++i) {
      float closestDepth = texture(depthMap, fragToLight + gridSamplingDisk[i] * diskRadius).r;
      closestDepth *= far_plane;
      if(currentDepth - bias > closestDepth)
      shadow += 1.0;
   }
   shadow /= float(samples);

   return shadow;
}

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir, float shadow) {
   vec3 lightDir = normalize(light.position - fragPos);

   float diff = max(dot(normal, lightDir), 0.0);

   vec3 reflectDir = reflect(-lightDir, normal);
   float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);

   float distance = length(light.position - fragPos);
   float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

   vec3 ambient = light.ambient * vec3(texture(material.texture_diffuse1, TexCoords));
   vec3 diffuse = light.diffuse * diff * vec3(texture(material.texture_diffuse1, TexCoords));
   vec3 specular = light.specular * spec * vec3(texture(material.texture_specular1, TexCoords).xxx);

   ambient *= attenuation;
   diffuse *= attenuation;
   specular *= attenuation;

   return ambient + (1.0 - shadow) * ( diffuse + specular);
}

void main() {
   vec3 normal = normalize(Normal);
   vec3 viewDir = normalize(viewPosition - FragPos);
   float shadow = ShadowCalculation(FragPos);
   vec3 result = CalcPointLight(pointLight, normal, FragPos, viewDir, shadow);

   float brightness = dot(result, vec3(0.2126, 0.7152, 0.0722));
   if(brightness > 0.2) {
      BrightColor = vec4(result, 1.0);
   } else {
      BrightColor = vec4(0.0, 0.0, 0.0, 1.0);
   }

   FragColor = vec4(result, 1.0);
}
