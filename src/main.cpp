#include <gl/project.hpp>
#include <iostream>

#include "scenes/main_menu/main_menu.hpp"

const std::string name = "Game Night";
constexpr int width    = 800;
constexpr int height   = 600;

int main([[maybe_unused]] const int argc, [[maybe_unused]] char **argv) try {
    gl::Project::init(name, width, height);
    gl::Project::set_scene(new MainMenu());

    gl::Project::run();

    gl::Project::finalize();

    return 0;
} catch (const std::exception &e) {
    std::cerr << "ERROR: " << e.what() << '\n';
    exit(1);
}
