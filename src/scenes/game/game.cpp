#include "game.hpp"

#include <imgui.h>

#include <array>
#include <gl/camera.hpp>
#include <gl/lights.hpp>
#include <gl/mesh_instance.hpp>
#include <gl/model.hpp>
#include <gl/project.hpp>

bool GameScene::menu_open      = false;
bool GameScene::camera_enabled = true;

gl::PlayerCamera *camera       = nullptr;
gl::PointLight *lightNode      = nullptr;
gl::MeshInstance *spinning_top = nullptr;

gl::Program *GameScene::build_shaders() {
    auto shaders = std::vector<gl::Shader>();
    shaders.reserve(2);
    shaders.emplace_back(DATADIR "/shaders/shader.vert", GL_VERTEX_SHADER);
    shaders.emplace_back(DATADIR "/shaders/shader.frag", GL_FRAGMENT_SHADER);

    return new gl::Program(shaders);
}

GameScene::GameScene() : Scene(build_shaders(), true, true, true) {
    clear_color = {0.1, 0.1, 0.1};

    // clang-format off
    this->set_children({
        gl::MeshInstance::builder()
            ->name("room")
            ->position({-1, -2.2, -5})
            ->scale(glm::vec3(1))
            ->model(
                gl::Model::builder()
                    ->path(DATADIR "/objects/room/room.gltf")
                    ->reference_point(CENTER, BACK, CENTER)
                    ->build()
            )
            ->build(),

        gl::MeshInstance::builder()
            ->name("table")
            ->position({0, 0, 0})
            ->scale(glm::vec3 (3))
            ->model(
                gl::Model::builder()
                    ->path(DATADIR "/objects/table/table.gltf")
                    ->reference_point(CENTER, FRONT, CENTER)
                    ->build()
            )
            ->children({
                camera = gl::PlayerCamera::builder()
                    ->active(true)
                    ->position({-5, 3, -3})
                    ->rotation(glm::vec3(-0.4, -2.2, 0))
                    ->build(),

                gl::MeshInstance::builder()
                    ->name("table")
                    ->position({-0.2, 0.1, 0})
                    ->rotation(glm::vec3(0, 3.14 * (1.0/2), 0))
                    ->scale(glm::vec3(0.15))
                    ->model(
                        gl::Model::builder()
                            ->path(DATADIR "/objects/table/table.gltf")
                            ->reference_point(CENTER, FRONT, CENTER)
                            ->build()
                    )
                    ->children({
                        gl::MeshInstance::builder()
                            ->name("cards")
                            ->position({0, 0.01, 0})
                            ->scale(glm::vec3 (1.75))
                            ->model(
                                gl::Model::builder()
                                    ->path(DATADIR "/objects/cards/cards.gltf")
                                    ->reference_point(CENTER, BACK, CENTER)
                                    ->build()
                            )
                            ->build(),
                    })
                ->build(),

                gl::MeshInstance::builder()
                    ->name("dino")
                    ->position({-0.1, 0, 0.3})
                    ->rotation(glm::vec3(0, 3.14 * (6.0/5), 0))
                    ->scale(glm::vec3(0.002))
                    ->model(
                        gl::Model::builder()
                            ->path(DATADIR "/objects/dino/scene.gltf")
                            ->reference_point(CENTER, BACK, CENTER)
                            ->build()
                    )
                    ->build(),

                    gl::MeshInstance::builder()
                        ->name("plush_octopus1")
                        ->position({0.1, 0, 0})
                        ->rotation(glm::vec3(0, 3.14 * (-2.0/5), 0))
                        ->scale(glm::vec3(1))
                        ->model(
                            gl::Model::builder()
                                ->path(DATADIR "/objects/plush1/plush.gltf")
                                ->reference_point(CENTER, BACK, CENTER)
                                ->build()
                        )
                        ->build(),

                    gl::MeshInstance::builder()
                        ->name("plush_octopus2")
                        ->position({-0.2, 0, -0.35})
                        ->scale(glm::vec3(1))
                        ->model(
                            gl::Model::builder()
                                ->path(DATADIR "/objects/plush2/plush.gltf")
                                ->reference_point(CENTER, BACK, CENTER)
                                ->build()
                        )
                        ->build(),

                    gl::MeshInstance::builder()
                        ->name("ducky")
                        ->position({-0.35, 0, 0.3})
                        ->rotation(glm::vec3(0, 3.14 * (4.0/4), 0))
                        ->scale(glm::vec3(0.8))
                        ->model(
                            gl::Model::builder()
                                ->path(DATADIR "/objects/ducky/ducky.gltf")
                                ->reference_point(CENTER, BACK, CENTER)
                                ->build()
                        )
                        ->build(),

                    gl::MeshInstance::builder()
                        ->name("frog")
                        ->position({-0.45, 0, 0})
                        ->rotation(glm::vec3(0, 3.14 * (1.0/2), 0))
                        ->scale(glm::vec3(0.5))
                        ->model(
                            gl::Model::builder()
                                ->path(DATADIR "/objects/frog/frog.gltf")
                                ->reference_point(CENTER, BACK, CENTER)
                                ->build()
                        )
                        ->build(),

                    spinning_top = gl::MeshInstance::builder()
                        ->name("spinning_top")
                        ->position({-0.8, 0, 0})
                        ->scale(glm::vec3(0.2))
                        ->model(
                            gl::Model::builder()
                                ->path(DATADIR "/objects/spinning_top/spinning_top.gltf")
                                ->reference_point(CENTER, BACK, CENTER)
                                ->build()
                        )
                        ->build(),

                    gl::MeshInstance::builder()
                        ->name("lamp")
                        ->position({-0.8, 0, 0.45})
                        ->rotation(glm::vec3(0, 3.14 * (3.0/4), 0))
                        ->scale(glm::vec3(0.2))
                        ->model(
                            gl::Model::builder()
                                ->path(DATADIR "/objects/lamp/lamp.gltf")
                                ->reference_point(CENTER, BACK, CENTER)
                                ->build()
                        )
                        ->children({
                            lightNode = gl::PointLight::builder()
                                ->name("light")
                                ->position({0, 1, 0.5})
                                ->ambient({0.5, 0.5, 0.5})
                                ->diffuse({2, 2, 2})
                                ->specular({0.9, 0.9, 0.9})
                                ->constant(1.0)
                                ->linear(0.5)
                                ->quadratic(1.0)
                                ->build(),
                        })
                        ->build(),

                    gl::MeshInstance::builder()
                        ->name("books")
                        ->position({0.7, -0.01, 0})
                        ->rotation(glm::vec3(0, -3.14/2, 0))
                        ->model(
                            gl::Model::builder()
                                ->path(DATADIR "/objects/books/books.gltf")
                                ->reference_point(CENTER, BACK, CENTER)
                                ->build()
                        )

                        ->children({
                            gl::MeshInstance::builder()
                                ->name("fox")
                                ->position({0, 0.01, 0.15})
                                ->rotation(glm::vec3(0, 3.14, 0))
                                ->model(
                                    gl::Model::builder()
                                        ->path(DATADIR "/objects/fox/fox.gltf")
                                        ->reference_point(CENTER, BACK, CENTER)
                                        ->build()
                                )
                                ->build(),
                        })
                        ->build(),
            })
            ->build(),
    });
    // clang-format on

    set_camera(camera);

    gl::Project::get_window()->set_input_mode(GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void GameScene::draw([[maybe_unused]] float delta, [[maybe_unused]] gl::Program *shader) {
    get_program()->use();
    get_program()->setVec3("pointLight.position", lightNode->get_world_position());
    get_program()->setVec3("pointLight.ambient", lightNode->get_ambient());
    get_program()->setVec3("pointLight.diffuse", lightNode->get_diffuse());
    get_program()->setVec3("pointLight.specular", lightNode->get_specular());
    get_program()->setFloat("pointLight.constant", lightNode->get_constant());
    get_program()->setFloat("pointLight.linear", lightNode->get_linear());
    get_program()->setFloat("pointLight.quadratic", lightNode->get_quadratic());

    get_program()->setFloat("material.shininess", 24.0f);
}

void GameScene::update([[maybe_unused]] float delta) {
    const static glm::vec3 spin_position = spinning_top->get_position();
    const auto time                      = gl::Window::current_time();

    spinning_top->rotate_around(spinning_top->up(), spinning_top->get_position(), 2000 * delta);
    spinning_top->set_position(spin_position + glm::vec3(0, 0, 0.1 * cos(time)) +
                               glm::vec3(0.1 * sin(2.0 * time) / 2.0, 0, 0));
}

void GameScene::draw_ui() {
    if (!menu_open) {
        return;
    }

    constexpr ImGuiWindowFlags window_flags =
        ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse |
        ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoSavedSettings;

    auto window_size          = gl::Window::get_size();
    std::array<float, 2> size = {static_cast<float>(std::min(500, window_size[0])),
                                 static_cast<float>(std::min(500, window_size[1]))};

    {
        ImGui::Begin("Pause menu", nullptr, window_flags);
        ImGui::SetWindowPos("Pause menu",
                            ImVec2((static_cast<float>(window_size[0]) - size[0]) / 2.0f,
                                   (static_cast<float>(window_size[1]) - size[1]) / 2.0f));
        ImGui::SetWindowSize("Pause menu", ImVec2(size[0], size[1]));

        ImGui::Text("Game is paused");

        if (ImGui::Button("Exit")) {
            gl::Project::close();
        }

        ImGui::End();
    }
}

void GameScene::key_pressed(double delta) {
    if (gl::Project::key_clicked(GLFW_KEY_W)) {
        camera->move(camera->front() * camera->get_movement_speed() * static_cast<float>(delta));
    }

    if (gl::Project::key_clicked(GLFW_KEY_S)) {
        camera->move(-camera->front() * camera->get_movement_speed() * static_cast<float>(delta));
    }

    if (gl::Project::key_clicked(GLFW_KEY_A)) {
        camera->move(-camera->right() * camera->get_movement_speed() * static_cast<float>(delta));
    }

    if (gl::Project::key_clicked(GLFW_KEY_D)) {
        camera->move(camera->right() * camera->get_movement_speed() * static_cast<float>(delta));
    }
}

void GameScene::key_clicked(int key, [[maybe_unused]] int scancode, int action, int mods) {
    if (action != GLFW_PRESS) {
        return;
    }

    switch (key) {
        case GLFW_KEY_ESCAPE:
            menu_open      = !menu_open;
            camera_enabled = !camera_enabled;

            gl::Project::get_window()->set_input_mode(
                GLFW_CURSOR, (menu_open) ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);

            break;
        case GLFW_KEY_Q:
            if (mods & GLFW_MOD_CONTROL) {
                gl::Project::close();
            }

            break;
        default:
            break;
    }
}

void GameScene::mouse_moved(double xpos, double ypos) {
    static bool first_move = true;
    static glm::vec2 position;

    if (first_move) {
        position.x = static_cast<float>(xpos);
        position.y = static_cast<float>(ypos);
        first_move = false;
    }

    if (camera_enabled) {
        const float xoffset = static_cast<float>(xpos) - position.x;
        const float yoffset = position.y - static_cast<float>(ypos);

        camera->process_movement(xoffset, yoffset);
    }

    position.x = static_cast<float>(xpos);
    position.y = static_cast<float>(ypos);
}
