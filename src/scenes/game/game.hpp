#ifndef GAME_H
#define GAME_H

#include <gl/model.hpp>
#include <gl/scene.hpp>
#include <gl/shader.hpp>

class GameScene : public gl::Scene {
    static bool menu_open;
    static bool camera_enabled;

   protected:
    void key_clicked(int key, int scancode, int action, int mods) override;
    void key_pressed(double delta) override;
    void mouse_moved(double xpos, double ypos) override;
    void draw_ui() override;
    void update(float delta) override;

   public:
    GameScene();
    ~GameScene() override = default;

    void draw(float delta, gl::Program *shader) override;
    std::string scene_name() override { return "game"; };

   private:
    static gl::Program *build_shaders();
};

#endif  // GAME_H
