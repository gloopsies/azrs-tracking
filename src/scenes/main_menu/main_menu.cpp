#include "main_menu.hpp"

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#include <array>
#include <gl/project.hpp>

#include "scenes/game/game.hpp"

void MainMenu::_draw([[maybe_unused]] float delta, [[maybe_unused]] gl::Program *shader) {
    glClearColor(0, 0, 0, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    constexpr ImGuiWindowFlags window_flags =
        ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse |
        ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoSavedSettings;

    auto window_size          = gl::Window::get_size();
    std::array<float, 2> size = {static_cast<float>(std::min(200, window_size[0])),
                                 static_cast<float>(std::min(200, window_size[1]))};
    {
        ImGui::Begin("Main menu", nullptr, window_flags);

        ImGui::SetWindowPos("Main menu",
                            ImVec2((static_cast<float>(window_size[0]) - size[0]) / 2.0f,
                                   (static_cast<float>(window_size[1]) - size[1]) / 2.0f));
        ImGui::SetWindowSize("Main menu", ImVec2(size[0], size[1]));

        if (ImGui::Button("Start")) {
            gl::Project::set_scene(new GameScene());
        }

        if (ImGui::Button("Exit")) {
            gl::Project::close();
        }

        ImGui::End();
    }

    //    ImGui::ShowDemoWindow();

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}