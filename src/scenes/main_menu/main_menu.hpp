#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include <gl/scene.hpp>
#include <string>

class MainMenu : public gl::Scene {
   public:
    MainMenu()           = default;
    ~MainMenu() override = default;

    void _draw(float delta, gl::Program *shader) override;
    std::string scene_name() override { return "main_menu"; };
};

#endif  // MAIN_MENU_H
