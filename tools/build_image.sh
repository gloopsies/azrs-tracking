#!/bin/bash

set -e

PROJECT_ROOT=${1:-"."}
. "$PROJECT_ROOT"/.env

$ENGINE build -t "$IMAGE_NAME"/build:"$VERSION" "$PROJECT_ROOT" --file "containers/build.Dockerfile"
$ENGINE build -t "$IMAGE_NAME":"$VERSION" "$PROJECT_ROOT" --file "containers/game_night.Dockerfile"