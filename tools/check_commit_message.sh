#!/usr/bin/sh

set -e

commit_types="(feature|bugfix|refactor|style|chore|wip)"
commit_regex="^${commit_types}"': .{20,500}'

if ! grep -E "$commit_regex" "$1" > /dev/null; then
  echo 'Your git commit message:'
  echo '```'
  cat "$1"
  echo '```'
  echo 'does not comform to the project standards'

  exit 1
fi
