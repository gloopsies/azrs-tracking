#!/usr/bin/sh

set -e

PROJECT_ROOT=${1:-"."}
. "$PROJECT_ROOT"/.env
cd "$PROJECT_ROOT"

FILES_TO_CHECK='^./(src|libs/gl)/.*$'

CLANG_FORMAT_PATH=$(which clang-format 2> /dev/null)
if [ -z "$CLANG_FORMAT_PATH" ]; then
    echo "Error: \`clang-format\` not available, can't check file formatting"
    exit 1
fi

for file in $(find . | grep -E "$FILES_TO_CHECK" | grep -E '\.(cpp|cc|h|hpp)$'); do
    clang-format --dry-run -Werror "$file"
done
