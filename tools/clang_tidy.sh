#!/usr/bin/sh

set -e

PROJECT_ROOT=${1:-"."}
. "$PROJECT_ROOT"/.env
cd "$PROJECT_ROOT"

FILES_TO_CHECK='^./(src|libs/gl)/.*$'

CLANG_TIDY_PATH=$(which clang-tidy 2> /dev/null)
if [ -z "$CLANG_TIDY_PATH" ]; then
    echo "Error: \`clang-tidy\` not available, can't check files"
    exit 1
fi

pids=""
for file in $(find . | grep -E "$FILES_TO_CHECK" | grep -E '\.(cpp|cc|h|hpp)$'); do
    clang-tidy "$file" -p "$BUILD_DIR/compile_commands.json" 2> /dev/null &
    pids="$pids:$!"
done

OLDIFS="$IFS"
IFS=:
for pid in $pids; do
    if [ -z "$pid" ]; then
        continue
    fi

    wait "$pid"
done
IFS="$OLDIFS"
