#!/bin/bash

set -e

PROJECT_ROOT=${1:-"."}
. "$PROJECT_ROOT"/.env

args=(
    --security-opt label=disable
    -v /tmp/.X11-unix/:/tmp/.X11-unix/
    -e DISPLAY="$DISPLAY"
    -v /run/user/1000/:/run/user/1000/
    -e XDG_RUNTIME_DIR=/run/user/1000
    -e PULSE_SERVER=/run/user/1000/pulse/native
    --ipc host
    --userns host
    --network host
)

$ENGINE create "${args[@]}" --name "$CONTAINER_NAME" "$IMAGE_NAME":"$VERSION"
