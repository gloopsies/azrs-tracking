#!/usr/bin/sh

set -e

PROJECT_ROOT=${1:-"."}
. "$PROJECT_ROOT"/.env
cd "$PROJECT_ROOT"

FILES_TO_CHECK="src libs/gl"

for file in $(find $FILES_TO_CHECK | grep -E '\.(cpp|cc|h|hpp)$'); do
    sed -Ei 's/catch \(([a-zA-Z0-9_(::)]+) ?& ?([a-zA-Z0-9_]*)\)/catch (const \1 \&\2)/' "$file"
done
