#!/bin/sh

set -e

PROJECT_ROOT=${1:-"."}
. "$PROJECT_ROOT"/.env

cp hooks/* .git/hooks

mkdir -p "$BUILD_DIR"

env BUILD_DIR="$BUILD_DIR" conan install . --output-folder="$BUILD_DIR" --build=missing

meson setup --reconfigure --native-file "$BUILD_DIR"/conan_meson_native.ini . "$BUILD_DIR"
